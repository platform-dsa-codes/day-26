import java.io.*;
import java.util.*;

  public class Main {

    public static void main(String[] args) throws Exception {
        BufferedReader br =
            new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(br.readLine().trim());
        while (tc-- > 0) {
            String[] inputLine;
            inputLine = br.readLine().trim().split(" ");
            int k = Integer.parseInt(inputLine[0]);
            inputLine = br.readLine().trim().split(" ");
            int n = Integer.parseInt(inputLine[0]);
            int[] arr = new int[n];
            inputLine = br.readLine().trim().split(" ");
            for (int i = 0; i < n; i++) {
                arr[i] = Integer.parseInt(inputLine[i]);
            }

            int ans = new Solution().getMinDiff(arr, n, k);
            System.out.println(ans);
        }
    }
}


class Solution {
    int getMinDiff(int[] arr, int n, int k) {
        // Sort the array to efficiently determine min & max heights
        Arrays.sort(arr);

        // Initialize min difference
        int minDiff = Integer.MAX_VALUE;

        // Iterate through possible boundary points (i) between increasing and decreasing towers
        for (int i = 1; i < n; i++) {
            // Calculate potential minimum and maximum heights after modification
            int minHeight = Math.min(arr[0] + k, arr[i] - k);
            int maxHeight = Math.max(arr[i - 1] + k, arr[n - 1] - k);

            // Ensure non-negative heights after modification
            if (minHeight >= 0 && maxHeight >= 0) {
                // Update min difference if smaller
                minDiff = Math.min(minDiff, maxHeight - minHeight);
            }
        }

        // Handle the special case where all towers are increased (i = n)
        minDiff = Math.min(minDiff, arr[n - 1] - arr[0]);

        return minDiff;
    }
}
